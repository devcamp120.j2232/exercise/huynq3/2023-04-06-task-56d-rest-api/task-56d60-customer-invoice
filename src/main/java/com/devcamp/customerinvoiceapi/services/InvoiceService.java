package com.devcamp.customerinvoiceapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customerinvoiceapi.models.Invoice;
@Service
public class InvoiceService extends CustomerService{
    Invoice invoice1 = new Invoice(101, customer1, 10000  );
    Invoice invoice2 = new Invoice(102, customer2, 20000 );
    Invoice invoice3 = new Invoice(103, customer3, 30000 );
    Invoice invoice4 = new Invoice(104, customer1, 40000 );
    Invoice invoice5 = new Invoice(105, customer2, 50000 );
    Invoice invoice6 = new Invoice(106, customer3, 60000 );
    public ArrayList<Invoice> getAllInvoices(){
        ArrayList<Invoice> invoiceList = new ArrayList<>();
        invoiceList.add(invoice1);
        invoiceList.add(invoice2);
        invoiceList.add(invoice3);
        invoiceList.add(invoice4);
        invoiceList.add(invoice5);
        invoiceList.add(invoice6);
        return invoiceList;
    }
    public Invoice getInvoiceById(int index){
        Invoice invoice = null;
        if (index>=0 && index < getAllInvoices().size()){
            invoice = getAllInvoices().get(index);
        }
        return invoice;
    }
}
